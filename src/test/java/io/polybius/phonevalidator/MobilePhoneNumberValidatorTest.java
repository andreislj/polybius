package io.polybius.phonevalidator;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import java.util.List;
import org.junit.Test;

public class MobilePhoneNumberValidatorTest {

  private MobilePhoneNumberValidator validator = new MobilePhoneNumberValidator();

  @Test
  public void validate() {

    ValidationResultDto result = validator.validate(List.of("xxxx", "+370-612-34-567"));

    assertEquals(List.of("+370-612-34-567"), result.getValidPhones(Country.LITHUANIA));
    assertEquals(List.of("xxxx"), result.invalidPhones);

  }

  @Test
  public void validateLT() {

    ValidationResultDto result = validator.validate(List.of("+37061234567", "+370-612-34-567"));
    assertEquals(List.of("+37061234567", "+370-612-34-567"), result.getValidPhones(Country.LITHUANIA));

    result = validator.validate(List.of("+37091234567"));
    assertEquals(List.of("+37091234567"), result.invalidPhones);

    result = validator.validate(List.of("+3706123456"));
    assertEquals(List.of("+3706123456"), result.invalidPhones);
  }

  @Test
  public void validateLV() {

    ValidationResultDto result = validator.validate(List.of("37121234567"));
    assertEquals(List.of("37121234567"), result.getValidPhones(Country.LATVIA));

    result = validator.validate(List.of("37131234567"));
    assertNull(result.getValidPhones(Country.LATVIA));
    assertEquals(List.of("37131234567"), result.invalidPhones);

  }

  @Test
  public void validateEE() {

    ValidationResultDto result = validator.validate(List.of("3725123456", "37251234567"));
    assertEquals(List.of("3725123456", "37251234567"), result.getValidPhones(Country.ESTONIA));

  }

  @Test
  public void validateBE() {

    ValidationResultDto result = validator.validate(List.of("32456123456", "+32456123456"));
    assertEquals(List.of("32456123456", "+32456123456"), result.getValidPhones(Country.BELGIUM));

    result = validator.validate(List.of("324561234567"));
    assertNull(result.getValidPhones(Country.BELGIUM));
    assertEquals(List.of("324561234567"), result.invalidPhones);

  }

}