package io.polybius.phonevalidator;

public enum Country {

  LATVIA("LV"),
  LITHUANIA("LT"),
  ESTONIA("EE"),
  BELGIUM("BE");

  //ISO country code
  private final String code;

  Country(final String code) {
    this.code = code;
  }
}
