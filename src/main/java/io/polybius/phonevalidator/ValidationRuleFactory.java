package io.polybius.phonevalidator;

import java.util.List;
import java.util.Set;

public class ValidationRuleFactory {

  private static final List<ValidationRule> validationRules = List.of(
      new ValidationRule(Country.LITHUANIA, "370", Set.of("6"), Set.of(8)),
      new ValidationRule(Country.LATVIA, "371", Set.of("2"), Set.of(8)),
      new ValidationRule(Country.ESTONIA, "372", Set.of("5"), Set.of(7, 8)),
      new ValidationRule(Country.BELGIUM, "32", Set.of("456", "47", "48", "49"), Set.of(9)));

  public static ValidationAndCallingCode find(final String phoneNumber) {

    for (final ValidationRule vr : validationRules) {
      final String callingCode = vr.extractCallingCode(phoneNumber);
      if (callingCode != null) {
        return new ValidationAndCallingCode(vr, callingCode);
      }

    }

    return null;

  }

  static class ValidationAndCallingCode {

    public ValidationRule validationRule;
    public String callingCode;

    public ValidationAndCallingCode(ValidationRule validationRule, String callingCode) {
      this.validationRule = validationRule;
      this.callingCode = callingCode;
    }
  }

}
