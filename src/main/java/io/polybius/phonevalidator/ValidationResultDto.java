package io.polybius.phonevalidator;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ValidationResultDto {

  private Map<Country, List<String>> validPhonesByCountry = new HashMap<>();

  List<String> invalidPhones = new ArrayList<>();

  public void addValidPhone(final Country country, final String phone) {
    validPhonesByCountry.computeIfAbsent(country, _country -> new ArrayList<>()).add(phone);
  }

  public List<String> getValidPhones(final Country country) {
    return validPhonesByCountry.get(country);
  }

}
