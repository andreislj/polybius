package io.polybius.phonevalidator;

import io.polybius.phonevalidator.ValidationRuleFactory.ValidationAndCallingCode;
import java.util.List;

public class MobilePhoneNumberValidator {

  public ValidationResultDto validate(final List<String> phoneNumbers) {

    final ValidationResultDto result = new ValidationResultDto();

    for (final String rawPhoneNumber : phoneNumbers) {

      final String phoneNumber = rawPhoneNumber.replaceAll("\\)", "").replaceAll("\\(", "").replaceAll(" ", "").replaceAll("-", "");
      final ValidationAndCallingCode validation = ValidationRuleFactory.find(phoneNumber);
      final boolean isValid = validation != null && validation.validationRule.isValid(phoneNumber, validation.callingCode);

      if (isValid) {
        result.addValidPhone(validation.validationRule.getCountry(), rawPhoneNumber);
      } else {
        result.invalidPhones.add(rawPhoneNumber);
      }

    }

    return result;
  }


}
