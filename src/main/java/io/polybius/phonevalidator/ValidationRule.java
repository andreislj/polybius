package io.polybius.phonevalidator;

import java.util.Set;

public class ValidationRule {

  final Country country;
  final String callingCode;
  final Set<String> startsWith;
  final Set<Integer> lengths;

  public ValidationRule(final Country country, final String callingCode, final Set<String> startsWith, final Set<Integer> lengths) {
    this.country = country;
    this.callingCode = callingCode;
    this.startsWith = startsWith;
    this.lengths = lengths;
  }

  public Country getCountry() {
    return country;
  }

  public boolean isValid(final String phoneNumber, final String actualCallingCode) {

    final String base = phoneNumber.substring(actualCallingCode.length());
    for (final String sw : startsWith) {
      if (base.startsWith(sw)) {
        return lengths.contains(base.length());
      }

    }

    return false;
  }

  public String extractCallingCode(final String phoneNumber) {

    if (phoneNumber.startsWith(callingCode)) {
      return callingCode;
    } else if (phoneNumber.startsWith("+" + callingCode)) {
      return "+" + callingCode;
    }

    return null;
  }

}
